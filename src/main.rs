#[macro_use] extern crate gfx;
// At this time, I have honestly no idea which of these are necessary
// and which are redundant.
use glutin;
use glutin::WindowEvent::*;
use image;
use gfx_window_glutin;
use gfx_device_gl as gfx_device;
use gfx::traits::FactoryExt;
use gfx::memory::{Bind, Usage, Typed};
use gfx::texture::{Kind, AaMode, ImageInfoCommon};
use gfx::format::{
    Rgba8,
    DepthStencil,
    R8_G8_B8_A8,
    ChannelType,
    Srgb,
    Format,
    SurfaceType,
    Swizzle,
};
use gfx::buffer::Role;
use gfx::pso::bundle::Bundle;
use gfx::{
    Factory,
    Device,
};

// Define our vertex and pipe. I haven't looked much into this, but these
// values are the ones that get passed into the shaders.
gfx_defines!{
    vertex Vertex{
        pos: [f32; 2] = "a_Pos",
        tex: [f32; 2] = "a_Tex",
    }

    pipeline pipe {
        vbuf: gfx::VertexBuffer<Vertex> = (),
        tex: gfx::TextureSampler<[f32; 4]> = "u_Source",
        // out: gfx::RenderTarget<Rgba8> = "Target0",
        // If we want to have transparent pixels (we do) we can't use
        // RenderTarget, but need BlendTarget with a bunch of stuff.
        out: gfx::BlendTarget<Rgba8> = (
            "Target0",
            gfx::state::ColorMask::all(),
            gfx::preset::blend::ALPHA
        ),
    }
}

// We need a clear color for anything outside our texture.
// Going to eventually make this black.
const CLEAR_COLOR: [f32; 4] = [1.0, 0.0, 1.0, 0.5];

// We construct surfaces using triangles -- so here we're going to create
// FOUR vertices, and create TWO triangles with them using indices.
// We're using normalized coordinates from -1 to 1 (opengl standard?)
// for positions, and 0 to 1 for textures.
const SCREEN: [Vertex; 4] = [
    Vertex {pos: [-1.0, -1.0], tex: [0.0, 1.0]},
    Vertex {pos: [1.0, -1.0], tex: [1.0, 1.0]},
    Vertex {pos: [1.0, 1.0], tex: [1.0, 0.0]},
    Vertex {pos: [-1.0, 1.0], tex: [0.0, 0.0]},
];
const SCREEN_INDICES: &[u16] = &[0, 1, 2, 0, 3, 2];


/// Our lovely 2D canvas
struct Gfx2DCanvas<R: gfx::Resources, C: gfx::CommandBuffer<R>> {
    window: glutin::GlWindow,
    device: gfx_device::Device,
    encoder: gfx::Encoder<R, C>,
    bundle: Bundle<R, pipe::Data<R>>,
    events_loop: glutin::EventsLoop,
    factory: gfx_device::Factory,
    upload_buffer: gfx::handle::Buffer<R, u8>,
    texture: gfx::handle::Texture<R, R8_G8_B8_A8>,
    texture_ratio: f32,
    image_info: ImageInfoCommon<Format>,

    depth_view: gfx::handle::DepthStencilView<R, DepthStencil>, 
    //target_view: gfx::handle::RenderTargetView<R, Rgba8>,
    //download_buffer: gfx::handle::Buffer<R, u8>,
}

// This is interesting -- we define our own canvas error enum for propagating
// errors from the underlying bits!
#[derive(Debug)]
enum Gfx2DCanvasError {
    UnableToCreatePSO(String),
    UnableToInitWindow(glutin::CreationError),
    UnableToCreateUBuf(gfx::buffer::CreationError),
    // UnableToCreateDBuf(gfx::buffer::CreationError),
    UnableToCreateTexture(gfx::texture::CreationError),
    UnableToUpdateTexture,
    UnableToViewTexture(gfx::ResourceViewError)
}

impl Gfx2DCanvas<gfx_device::Resources, gfx_device::CommandBuffer>{
    pub fn new(
        window_builder: glutin::WindowBuilder,
        texture_dimensions: (u16, u16)
    ) -> Result<Gfx2DCanvas<gfx_device::Resources, gfx_device::CommandBuffer>, Gfx2DCanvasError>{
        let (width, height) = texture_dimensions;

        // The events loop will catch things like keypresses and resizing. It's
        // going to handle ALL our input
        let events_loop = glutin::EventsLoop::new();
        let context_builder = glutin::ContextBuilder::new();

        // Generate the basics with our builder
        // This will yield a window (actual window), a device (virtual)
        // and a factory which will be used to do basically everything else.
        let (window, device, mut factory, target_view, depth_view) =
            match gfx_window_glutin::init::<Rgba8, DepthStencil>(
                window_builder,
                context_builder,
                &events_loop
            ){
                Ok(x) => x,
                Err(e) => return Err(Gfx2DCanvasError::UnableToInitWindow(e))
            };

        // PSO stands for Pipeline State Objects -- apparently modern graphics such
        // as Vulkan use this, and it's pretty important. We make some shaders (
        // they're just stolen) and end up with a pipe! Our above definition of
        // pipe and vertex will end up used here.
        let pso = match factory.create_pipeline_simple(
            include_bytes!("shaders/screen.glslv"),
            include_bytes!("shaders/screen.glslf"),
            pipe::new()
        ){
            Ok(x) => x,
            Err(e) => return Err(Gfx2DCanvasError::UnableToCreatePSO(e.to_string()))
        };

        // This is our encoder, we use it to talk to the commanbuffer. It will send
        // commands like 'draw' and 'clear'
        let encoder: gfx::Encoder<_, _> = factory.create_command_buffer().into();

        // Create a texture
        let texture = match factory.create_texture::<R8_G8_B8_A8>(
            Kind::D2(width as u16, height as u16, AaMode::Single),
            1,
            Bind::all(),
            Usage::Dynamic,
            Some(ChannelType::Srgb)
        ) {
            Ok(x) => x,
            Err(e) => return Err(Gfx2DCanvasError::UnableToCreateTexture(e))
        };

        let shader_r_view =
            match factory.view_texture_as_shader_resource::<(R8_G8_B8_A8, Srgb)>(&texture, (0,0), Swizzle::new()) {
                Ok(x) => x,
                Err(e) => return Err(Gfx2DCanvasError::UnableToViewTexture(e))
            };

        // This creates what we need for rendering. Takes our vertices and indices
        // and gives us a slice and a buffer.
        // I'm not sure of the technical details yet.
        let (vertex_buffer, slice) =
            factory.create_vertex_buffer_with_slice(&SCREEN, SCREEN_INDICES);
        
        let data = pipe::Data{
            vbuf: vertex_buffer,
            tex: (shader_r_view, factory.create_sampler(
                gfx::texture::SamplerInfo::new(
                    gfx::texture::FilterMethod::Scale,
                    gfx::texture::WrapMode::Clamp
            ))),
            out: target_view
        };
        // Bundle together our slice, pipeline, and data objects for
        // easy digestion.
        let bundle = Bundle { slice, pso, data};

        // This will be our buffer for updating images
        let upload_buffer = match factory.create_buffer::<u8>(
            (height * width * 4) as usize,
            Role::Staging,
            Usage::Dynamic,
            Bind::all()
        ){
            Ok(x) => x,
            Err(e) => return Err(Gfx2DCanvasError::UnableToCreateUBuf(e))
        };

        // Don't really know what this is for, but it gets used when
        // updating the texture.
        let image_info: ImageInfoCommon<Format> = ImageInfoCommon{
            xoffset: 0,
            yoffset: 0,
            zoffset: 0,
            width: width as u16,
            height: height as u16,
            depth: 0,
            format: Format(SurfaceType::R8_G8_B8_A8, ChannelType::Srgb),
            mipmap: 0,
        };

        Ok(Gfx2DCanvas{
            window,
            device,
            encoder,
            bundle,
            events_loop,
            factory,
            texture,
            texture_ratio: width as f32 / height as f32,
            image_info,
            upload_buffer,
            depth_view
        })
    }

    pub fn poll_events<F>(&mut self, callback: F) where F: FnMut(glutin::Event){
        self.events_loop.poll_events(callback) 
    }

    pub fn update_window(&mut self){
        // Need to recalculate slice and vbuf
        let size = self.window.get_inner_size().unwrap();

        // The majority of this code is to keep our canvas centered
        // and maintain aspect ratio.
        let window_ratio = size.width as f32 / size.height as f32;
        let new_ratio = window_ratio / self.texture_ratio;
        let (w, h);
        if new_ratio > 1.0 {
            w = 1.0 / new_ratio;
            h = 1.0;
        } else {
            w = 1.0;
            h = 1.0 * new_ratio;
        }
        let new_verts: [Vertex; 4] = [
            Vertex {pos: [-w, -h], tex: [0.0, 1.0]},
            Vertex {pos: [w, -h], tex: [1.0, 1.0]},
            Vertex {pos: [w, h], tex: [1.0, 0.0]},
            Vertex {pos: [-w, h], tex: [0.0, 0.0]},
        ];
        let (vbuf, slice) = 
            self.factory.create_vertex_buffer_with_slice(&new_verts, SCREEN_INDICES);

        self.bundle.slice = slice;
        self.bundle.data.vbuf = vbuf;

        gfx_window_glutin::update_views(
            &self.window, &mut self.bundle.data.out, &mut self.depth_view
        )
    }

    /// Given an array of RGBA pixels, update our texture
    pub fn update_texture(&mut self, img: &[u8]) -> Result<(), Gfx2DCanvasError>{
        match self.encoder.update_buffer(&self.upload_buffer, img, 0){
            Ok(_) => (),
            Err(_) => return Err(Gfx2DCanvasError::UnableToUpdateTexture)
        };

        // I definitely don't fully understand this bit, or why we use raw
        // over not raw. This is just ripped from an example.
        match self.encoder.copy_buffer_to_texture_raw(
            &self.upload_buffer.raw(),
            0,
            &self.texture.raw(),
            None,
            self.image_info.clone()
        ){
            Ok(_) => Ok(()),
            Err(_) => return Err(Gfx2DCanvasError::UnableToUpdateTexture)
        }
    } 

    /// Take our current bundle and draw it to the window
    pub fn draw(&mut self){
        self.encoder.clear(&self.bundle.data.out, CLEAR_COLOR);
        self.encoder.draw(&self.bundle.slice, &self.bundle.pso, &self.bundle.data);
        self.encoder.flush(&mut self.device);
        self.window.swap_buffers().unwrap();
    }

    pub fn cleanup(&mut self){
        self.device.cleanup()
    }
}


fn main() {
    // We're going to force x11 on unix because wayland basically just
    // breaks.
    std::env::set_var("WINIT_UNIX_BACKEND", "x11");
    let window_size = (600.0, 600.0);
    // Create a window builder. Glutin documentation details the various
    // available functions for this.
    let window_builder = glutin::WindowBuilder::new()
        .with_title("Texture")
        .with_resizable(true)
        .with_decorations(true)
        // Using .into() here 
        .with_dimensions(window_size.into());

    // Create our canvas...
    let mut canvas = Gfx2DCanvas::new(window_builder, (6, 6)).unwrap();

    //let img = image::open("src/media/square.jpg").unwrap().to_rgba();
    // We're going to create pixels here as a demonstration
    let img: [u8; 144] = [
        0,0,0,0, 0,0,0,0, 0,0,0,255, 0,0,0,255, 0,0,0,0, 0,0,0,0,
        0,0,0,0, 0,0,0,255, 255,255,255,255, 0,0,0,255, 255,255,255,255, 0,0,0,0,
        0,0,0,255, 0,0,0,255, 0,0,0,255, 0,0,0,255, 255,255,255,255, 255,255,255,255, 
        0,0,0,255, 0,0,0,255, 255,255,255,255, 255,255,255,255, 255,255,255,255, 255,255,255,255,
        0,0,0,0, 0,0,0,255, 255,255,255,255, 0,0,0,255, 255,255,255,255, 0,0,0,0,
        0,0,0,0, 0,0,0,0, 255,255,255,255, 255,255,255,255, 0,0,0,0, 0,0,0,0
    ];
    let mut running = true;
    let mut needs_window_update = false;
    while running {
        // We'll look for resizing and escape keys ~
        canvas.poll_events(|event| {
            // The .. is new syntax to me, but very cool -- it's used for
            // pattern matching, afaict, and just means 'whatever else'
            if let glutin::Event::WindowEvent {event, ..} = event{
                match event {
                    CloseRequested | KeyboardInput{
                        input: glutin::KeyboardInput {
                            virtual_keycode: Some(glutin::VirtualKeyCode::Escape), ..
                        }, ..
                    } => running = false,
                    Resized(_)|HiDpiFactorChanged(_) => needs_window_update = true,
                    _ => {}
                }
            }
        });
        if needs_window_update {
          canvas.update_window();
          needs_window_update = false;
        };
        canvas.update_texture(&img).unwrap();
        canvas.draw();
    }
    canvas.cleanup();
}
