Gfx 2D Canvas Exercise
=========================


First dive into graphical programming. Uses Glutin and gfx-rs (pre-ll).


Much thanks to the individuals maintaining [this tutorial](https://suhr.github.io/gsgt/) and [this git repo](https://github.com/unpatched/internet-broadcasting-service).
